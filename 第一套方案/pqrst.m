%计算心率后的数据
data=importdata('result.txt');
M=data'
size(data)


% ----------------下面是小波变换过滤噪声-------------------------------------
TIME=linspace(0,18.67,1868);%时间
% 
% clear all;clc
% load(‘Audio_1_resample.mat’);
s=M
%整个信号的长度
N = numel(s);
%小波分解;
[c,l]=wavedec(s,7,'coif5'); %小波基为coif5,分解层数为7层
ca11=appcoef(c,l,'coif5',7); %获取低频信号
cd1=detcoef(c,l,1);
cd2=detcoef(c,l,2); %获取高频细节
cd3=detcoef(c,l,3);
cd4=detcoef(c,l,4);
cd5=detcoef(c,l,5);
cd6=detcoef(c,l,6);
cd7=detcoef(c,l,7);
sd1=zeros(1,length(cd1));
sd2=zeros(1,length(cd2)); %1-3层置0,4-7层用软阈值函数处理
sd3=zeros(1,length(cd3));
sd4=wthresh(cd4,'s',0.014);
sd5=wthresh(cd5,'s',0.014);
sd6=wthresh(cd6,'s',0.014);
sd7=wthresh(cd7,'s',0.014);
c2=[ca11,sd7,sd6,sd5,sd4,sd3,sd2,sd1];
s0=waverec(c2,l,'coif5'); %小波重构
figure;
subplot(211);plot(s);subplot(212);plot(s0);%画图

sprintf('-----------经过小波过滤噪声后的数据保存到硬盘---------------------')

fid=fopen('wt.txt','w');
% fprintf(fid,'%7s %9s\r\n','Radius','Area');%写入列名
fprintf(fid,'%8d\r\n',s0);%写入每列的数据
fclose(fid);

% ------------------------------------------------------------