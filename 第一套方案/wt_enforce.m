
clc;
% v1=[1,4,6,4,1,6];
% v2=[6,5,6,6,6,6]
% s=0;
% for i=1:length(v1)
% %     s=s+v(i);
%     index=find(v1==6)
% %     v(i)
% end
% disp(s);

% v2


% 加载小波变换后的数据(需要在峰值位置进行加强)
wt_data=importdata('wt.txt');
M=wt_data';
size(M);
plot(M);


%基线漂移滤波后的峰值所在时刻点的数据
peak_data=importdata('locs.txt');
peak_data=peak_data';%peak_data是心拍所在的时刻位置,并不是心拍数值


format short
TIME=linspace(0,18.67,1868);
% size(TIME)
% TIME(1859)
% peak_data(18)
% TIME(1859)-peak_data(18)<1e-5


final_data=M;%用作喜好增强前的初始化


for i=1:length(peak_data)
    sprintf('---------------------开始寻找下标记----------------------------------------')
    peak_data(i);
    index=find(abs(TIME-peak_data(i))<1e-5)%寻找时刻中农等于峰值时刻点的下标
    sprintf('---------------------一轮for循环结束，输出峰值数据----------------------------------------')
    final_data(index)%根据下标记获取"心拍数值"
    
    
    %对心拍附近数据进行修复.我们以峰值(心拍)位置为中心，修复周边的修复五个点，即 峰值(心拍)±0.02s范围修复
    final_data(index-2)=final_data(index-2)+25;
    final_data(index-1)=final_data(index-1)+45;
    final_data(index)=final_data(index)+70;
    final_data(index+1)=final_data(index+1)+45;
    final_data(index+2)=final_data(index+2)+25;
    
end    
plot(final_data)%这个就是qrs最终效果了。