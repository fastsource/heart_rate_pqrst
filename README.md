[第一份方案实验报告](https://yuchi.blog.csdn.net/article/details/107562819)

**第一种方案运行步骤:**


|  运行步骤   | 运行文件  |代码读取的文件|代码生成的文件|对应实验哪个部分?
|  ----  | ----  | ----  | ----  |----|
| 1  | heart_filter.m |data4.txt(上海北京大学微电子研究院提供)|①result.txt(巴特沃斯滤波器+零相移滤波器处理结果)<br>②locs.txt(峰值所在时刻数据)|实验第1部分(滤波+计算心率)
| 2  | pqrst.m|result.txt|wt.txt(小波变换结果)|实验第2部分(小波变换)
| 3| wt_enforce.m |①locs.txt<br>②wt.txt|无|实验第2部分(心拍恢复)


**注:**

可以跳过1、2两步,
直接运行第3步(wt_enforce.m)获得最终PQRST波形




[第二份方案实验报告(相对于一种方案，只修改了小波变换部分)](https://yuchi.blog.csdn.net/article/details/107624547)

**第二种方案运行步骤:**


|  运行步骤   | 运行文件  |代码读取的文件|代码生成的文件|对应实验哪个部分?
|  ----  | ----  | ----  | ----  |----|
| 1  |heart_filter.m |data4.txt(上海北京大学微电子研究院提供)|①result.txt(巴特沃斯滤波器+零相移滤波器处理结果)<br>②locs.txt(峰值所在时刻数据)|实验第1部分
| 2  |signal_filter.m |①result.txt<br>②locs.txt|无|实验第2部分(指数平滑+心拍恢复)


